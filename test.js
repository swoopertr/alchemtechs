let TechData = require('./src/Data/Technician/technician');
let emailHelper = require('./src/Helper/mailHelper');
let core = require('./src/Core');
async function runData(){
    let techs = await TechData.async.getAll();
    for(let i = 0; i < techs.length; i++){
        let item = techs[i];
        
        let fullName = item.Name.split(' ');
        let surname = fullName[fullName.length - 1];
        let name = fullName.length == 3 ? fullName[0] + ' ' + fullName[1] : fullName[0];
        item.Surname = surname;
        item.Name = name;
        await TechData.async.test_update(item);
        console.log(item);
    }   
}

async function mailing() {
    let techs = await TechData.async.getAllActive();
    /*let emails = [
        'burcin.yucebag@alchemlife.eu',
        'marketing@alchemlifetr.com',
        'barisakkol@gmail.com'
        //'tunc.kiral@gmail.com'
    ];*/

    let teplate_text = `Değerli Yol Arkadaşımız <strong>{{Name}} {{Surname}}</strong>,<br><br>
    Ekim 2022’de başladığımız <strong>AlchemLife Teknisyen Eğitim Projesinde</strong> ilk etabın sonuna geliyoruz.<br><br>
    Mayıs ayı içerisinde noter huzurunda gerçekleşecek çekilişimizi siz de canlı yayından takip edebileceksiniz.<br><br>
    İlerleyen günlerde detaylarını paylaşacağımız çekilişe katılmaya hak kazanmak için, aşağıdaki linke tıklayarak KVKK metnine <span style="color:red;font-size:16px"><strong>30.04.2023 tarihine kadar</strong></span> onaylamanız gerekmektedir.<br><br>
    Linki tıkladıktan sonra İsim-Soyisim, İletişim Numarası ve Email bilgilerinizi onaylamanız gerekmektedir.<br><br>
    Onaylamak için lütfen tıklayınız -> <a href="https://alchemlifeegitim.com/verifykvkk?hash={{hash}}">EVET</a><br><br>
    <br>
    Saygılarımızla, AlchemLife Türkiye<br>`;
    
    for(let i = 0; i < techs.length; i++){
        let tech = techs[i];
        if (tech.Email == '') {
            continue;
        }
        let tmp = teplate_text.replace('{{Name}}', tech.Name);
        tmp = tmp.replace('{{Surname}}', tech.Surname);
        tmp = tmp.replace('{{hash}}', tech.kvkkhash);

        await emailHelper.send_mail_async(tech.Email, 'AlchemLife KVKK Onayı', tmp);
        console.log(`Email Send to ${tech.Id} , Name:  ${tech.Name} ${tech.Surname}` );
        core.sleep(2000);
    }

    console.log('done');
}

mailing();

//runData();

