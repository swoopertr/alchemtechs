console.log('admin_fud_savepharmacy_page.js');
userPageType = 1;
check_login();

(function () {

    document.getElementById('btnUpdatePharmacy').addEventListener('click', function (e) {
        e.preventDefault();
        updatePharmacy(function(result){
            if(result != "0") {
                alert('Kayıt işlemi başarılı. Anasayfaya yönlendiriliyorsunuz.');
                window.location.href = '/admin/pharmacy-list';
            }
        });
    });

    document.getElementById('btnSendPassword').addEventListener('click', function (e) {
        sendPasswordEmail(function(result){ 
            alert(result);
        });
    });

    function sendPasswordEmail(cb){
        var token = util.cookie.get('token');
        var check_obj = [
            { name: 'token',  value: token },
            { name: 'userPageType',  value: userPageType }
        ];
        var post_obj = {
            id: $('#Id').val()
        };

        util.postJsonRequest('/admin/sendpharmacypasswordmail', post_obj, check_obj, function (result) {
            if (result != "0") {
                cb && cb(result);
            } 
        });
    }

    function updatePharmacy(cb) {
        let Id = $('#Id').val();
        let pharmacyName = $('#pharmacyName').val();
        let nameSurname = $('#nameSurname').val();
        let gsm = $('#gsm').val();
        let officePhone = $('#officePhone').val();
        let email = $('#email').val();
        let glncode = $('#glnCode').val();
        let country = $('#country').val();
        let city = $('#city').val();
        let adress = $('#adress').val();
        let town = $('#town').val();
        let coordinate = $('#coordinate').val();
        
        let status = $('#status').val();
        let password = $('#password').val();
        //let technician = $('#nameTechnician').val();

        let coordinates = coordinate.split(',');
        let lat = coordinates[0];
        let lng = coordinates[1];
        let fudid = $('#fudSelect').val();
        let post_obj = {
            Id,
            pharmacyName,
            nameSurname,
            officePhone,
            status,
            gsm,
            lng,
            lat,
            email,
            glncode,
            country,
            city,
            town,
            adress,
            password,
            fudid
            //technician
        };
        
       

        var token = util.cookie.get('token');
        var check_obj = [
            { name: 'token',  value: token },
            { name: 'userPageType',  value: userPageType }
        ];

        util.postJsonRequest('/admin/updatepharmacy', post_obj, check_obj, function (result) {
            if (result != "0") {
                cb && cb(result);
            } 
        });
    }

})();