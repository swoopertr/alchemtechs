var render = require('./../Middleware/render');
var view = require('./../Middleware/ViewPack');
var header = require('./../Middleware/header');
var userBusiness = require('./../Bussines/userBusiness');
var adminBussiness = require('./../Bussines/adminBusiness');
var pharmacyBusiness = require('./../Bussines/pharmacyBussiness');
var reportBussines = require('./../Bussines/reportBusiness');
var fudBusiness = require('./../Bussines/fudBussiness');
var technicianBusiness = require('./../Bussines/technicianBusiness');
var core = require('./../Core');
var url = require('url');
var formidable = require('formidable');
var fs = require('fs');
var settings = require('./../Config/setting');
var excelHelper = require('./../Helper/excelHelper');


var admin = {
    admin: function (req, res) {
        var cookies = core.parseCookies(req);
        if(!cookies){
            core.redirect(res, '/');
            return;            
        }
        var token = cookies.token;
        if(!token){
            core.redirect(res, '/');
            return;
        }
        userBusiness.checkToken(token, function(result){
            if(result == false){
                /*render.renderData(res, {
                    page: "admin",
                    auth: "fail"
                });*/
                core.redirect(res, '/');
            }else{
                render.renderHtml(res, view.views["admin"]["index"], {});
            }
             
        });
    },
    pharmacy_list: function (req, res) {
        var cookies = core.parseCookies(req);
        var token = cookies.token;

        userBusiness.checkToken(token, function(result){
            if(result == false){
                render.renderData(res, {
                    page: "admin",
                    auth: "fail"
                });
                core.redirect(res, '/login');
            }else{
                adminBussiness.getPharmacyList(function(result){
                    let tmpPharmacyList = [];
                    for (let i = 0; i < result.length; i++) {
                        let item = result[i];
                        if(item.Status != 3){
                            tmpPharmacyList.push(item);
                        }
                        
                    }                    
                    render.renderHtml(res, view.views["admin"]["pharmacy_list"], {
                        pharmacies: tmpPharmacyList
                    });
                });
            }
             
        });
    },
    deletePharmacy : function (req, res) {
        var cookies = core.parseCookies(req);
        var token = cookies.token;
        if (token == undefined) {
            core.redirect(res, '/login');
            return;
        }
       
        req.on('end', function(){
            pharmacyBusiness.deletePharmacy(req.formData.id, function(result){
                if(result){
                    render.renderData(res, { status: "success" });
                }else{
                    render.renderData(res, { status: "error" });
                }
            });
        });
 
    },
    pharmacy_edit: function(req, res){
        var cookies = core.parseCookies(req);
        var token = cookies.token;
        if (token == undefined) {
            core.redirect(res, '/login');
            return;
        }
         
        let qs = url.parse(req.url, true).query;
        let id = qs.id; //pharmacy id

        if(id == undefined){ //insert
           
        } else { // update
            pharmacyBusiness.getPharmacyById(id, (result)=>{
                fudBusiness.fuds((fud_list)=>{
                    var data = {
                        data: result[0],
                        fud_list
                    };
                    render.renderHtml(res, view.views["admin"]["pharmacy_edit"], data);
                });
                
            });
            
        }
    },
    pharmacy_edit_post: function(req, res){
        var cookies = core.parseCookies(req);
        var token = cookies.token;
        if (token == undefined) {
            core.redirect(res, '/login');
            return;
        }
        
        req.on('end', function(){

            pharmacyBusiness.updatePharmacy(req.formData, function(result){
                if(result){
                    render.renderData(res, { status: "success" });
                }else{
                    render.renderData(res, { status: "error" });
                }
            });
        });


    },
    pharmacy_send_password_post: function(req, res){
        var cookies = core.parseCookies(req);
        var token = cookies.token;
        if (token == undefined) {
            core.redirect(res, '/login');
            return;
        }
        
        req.on('end', function(){
            pharmacyBusiness.getPharmacyById(req.formData.id, function(result){
                if(result.length > 0){
                    pharmacyBusiness.sendPharmacyPasswordMail(result[0].email, result[0].Password, function(result){
                        render.renderData(res, { status: "success" });
                    });
                }else{
                    render.renderData(res, { status: "Account not found!!!" });
                }
            });
            
        });
    },
    fuds: async function(req, res){
        var cookies = core.parseCookies(req);
        var token = cookies.token;
        if (token == undefined) {
            core.redirect(res, '/login');
            return;
        }
        fudBusiness.fuds(function (result) {
            
            let tmpList= [];
            for (let i = 0; i < result.length; i++) {
                var item = result[i];
                if (item.Status != 3) {
                    tmpList.push(item);
                }
            }
            var data = {
                list: tmpList
            };
            render.renderHtml(res, view.views["admin"]["fud_list"], data);
        });
    },
    allgoals: function(req, res){
        var cookies = core.parseCookies(req);
        var token = cookies.token;
        if (token == undefined) {
            core.redirect(res, '/login');
            return;
        }


        userBusiness.checkToken(token, function(result){
            if(result == false){
                render.renderData(res, {
                    page: "admin",
                    auth: "fail"
                });
                core.redirect(res, '/login');
            }else{
                adminBussiness.getAvailableGoals(function(result){
                    let data= {
                        list:result
                    };
                    render.renderHtml(res, view.views["admin"]["allgoals"], data);
                });
                
            }
        });
    },
    goal_appoint: function(req, res){
        var cookies = core.parseCookies(req);
        var token = cookies.token;
        if (token == undefined) {
            core.redirect(res, '/login');
            return;
        }


        userBusiness.checkToken(token, function(result){
            if(result == false){
                render.renderData(res, {
                    page: "admin",
                    auth: "fail"
                });
                core.redirect(res, '/login');
            }else{
                adminBussiness.getPharmacyList(function(result){
                    let tmpPharmacyList = [];
                    for (let i = 0; i < result.length; i++) {
                        let item = result[i];
                        if(item.Status != 3){
                            tmpPharmacyList.push(item);
                        }
                        
                    }                    
                    render.renderHtml(res, view.views["admin"]["pharmacy_list"], {
                        pharmacies: tmpPharmacyList
                    });
                });
            }
        });
    },
    delete_goal: function(req, res){
        var cookies = core.parseCookies(req);
        var token = cookies.token;
        if (token == undefined) {
            core.redirect(res, '/login');
            return;
        }

        req.on('end', function(){
            userBusiness.checkToken(token, function(result){
                if(result == false){
                    render.renderData(res, {
                        page: "admin",
                        auth: "fail"
                    });
                    core.redirect(res, '/login');
                }else{
                    var goalId = req.formData.id;
                    adminBussiness.deleteGoal(goalId, function(result){
                        console.log(result);
                        render.renderData(res, { result });
                    });
                }
            }); 
        });   
    },

    add_goal: function(req, res){
        var cookies = core.parseCookies(req);
        var token = cookies.token;
        if (token == undefined) {
            core.redirect(res, '/login');
            return;
        } 
        
        userBusiness.checkToken(token, function(result){
            if(result == false){
                render.renderData(res, {
                    page: "admin",
                    auth: "fail"
                });
                core.redirect(res, '/login');
            }else{  
                render.renderHtml(res, view.views["admin"]["goal_add"], {});
            }
        });


    },
    fud_excel: function(req, res){
        var cookies = core.parseCookies(req);
        var token = cookies.token;
        if (token == undefined) {
            core.redirect(res, '/login');
            return;
        } 

        userBusiness.checkToken(token, function(result){
            if(result == false){
                render.renderData(res, {
                    page: "admin",
                    auth: "fail"
                });
                core.redirect(res, '/login');
            }else{  
                let qs = url.parse(req.url, true).query;
                
                reportBussines.getCheckinReport(qs.startDate, qs.endDate, async function (result) {
                    await excelHelper.GenerateExcelFileAsync('fud_rapor.xlsx','fud',result);
                    core.returnFile(settings.downloadFolder + 'fud_rapor.xlsx', res);    
                });
                
            }
        });
    },
    answers_excel : function (req, res) {

     var cookies = core.parseCookies(req);
        var token = cookies.token;
        if (token == undefined) {
            core.redirect(res, '/login');
            return;
        } 

        userBusiness.checkToken(token, function(result){
            if(result == false){
                render.renderData(res, {
                    page: "admin",
                    auth: "fail"
                });
                core.redirect(res, '/login');
            }else{  
                let qs = url.parse(req.url, true).query;
                if (!qs.startDate){
                    render.renderData(res, {missing_field: 'startDate parametresi eksik'});
                    return;
                }
                if (!qs.endDate){
                    render.renderData(res, {missing_field: 'endDate parametresi eksik'});
                    return;
                }

                reportBussines.getAnswerReport(qs.startDate, qs.endDate, async function (result) {
                    var data = {
                        list: result
                    };
                    let tmp_data = [];
                    for(let i = 0 ; i< data.list.length; i++){
                        let item = data.list[i];
                        let tmp = {
                            Urun: item.urun,
                            Eczane : item.eczane,
                            Teknisyen : item.teknisyen,
                            FUD: item.Name,
                            SoruSayisi: item.questioncount,
                            DogruCevap : item.CorrectAnswerCount,
                            TamamlanmaTarihi : item.StartedDateDate,
                            TamamlanmaSaati: item.StartedTime,
                            GonderimTarihi : item.CreatedAtDate,
                            GonderimSaati : item.CreatedAttime
                            
                        }
                        tmp_data.push(tmp);
                    }
                    await excelHelper.GenerateExcelFileAsync('cevap_rapor.xlsx','fud', tmp_data);
                    core.returnFile(settings.downloadFolder + 'cevap_rapor.xlsx', res);   
                }, function(err){
                    render.renderData(res, {error: err});
                    return;
                });     
            }
        });
    },
    fudpharmacytech_excel : function (req, res) {

        var cookies = core.parseCookies(req);
           var token = cookies.token;
           if (token == undefined) {
               core.redirect(res, '/login');
               return;
           } 
           
           userBusiness.checkToken(token, function(result){
               if(result == false){
                   render.renderData(res, {
                       page: "admin",
                       auth: "fail"
                   });
                   core.redirect(res, '/login');
               }else{  
   
                   reportBussines.fudPharmacyTech (async function (result) {
                       
                       await excelHelper.GenerateExcelFileAsync('fudPharmacyTech_rapor.xlsx','fud', result);
                       core.returnFile(settings.downloadFolder + 'fudPharmacyTech_rapor.xlsx', res);   
                   }, function(err){
                       render.renderData(res, {error: err});
                       return;
                   });     
               }
           });
       },
    get_pharmacy_technican_admin: function(req, res){
        var cookies = core.parseCookies(req);
        var token = cookies.token;
        if (token == undefined) {
            core.redirect(res, '/login');
            return;
        } 

        userBusiness.checkToken(token, function(result){
            if(result == false){
                render.renderData(res, {
                    page: "admin",
                    auth: "fail"
                });
                core.redirect(res, '/login');
            }else{  
                let qs = url.parse(req.url, true).query;
                technicianBusiness.getPharmacyId(parseInt(qs.id), function (result) {
                    let lastResult = [];
                    for (let i = 0; i < result.length; i++) {
                        let item = result[i];
                        if(item.Status != 3){
                            lastResult.push(item);
                        }
                    }
                    var data = {
                        list : lastResult,
                        pharmacyId: qs.id
                    };
                    render.renderHtml(res, view.views["admin"]["get_pharmacy_technican_admin"], data);
                });
            }
        });
    },
    edit_technician_admin: function (req, res) {
        var cookies = core.parseCookies(req);
        var token = cookies.token;
        if (token == undefined) {
            core.redirect(res, '/login');
            return;
        } 

        userBusiness.checkToken(token, async function(result){
            if(result == false){
                render.renderData(res, {
                    page: "admin",
                    auth: "fail"
                });
                core.redirect(res, '/login');
            }else{  
                let qs = url.parse(req.url, true).query;
                if(qs.pharmacyid == undefined){
                    core.redirect(res, '/admin');
                    return;
                }
                if(parseInt(qs.id)==0){
                    let pharmacy = await pharmacyBusiness.getPharmacyByIdAsync(qs.pharmacyid);
                    if(pharmacy.length == 0){
                        core.redirect(res, '/admin');
                        return;    
                    }
                    var data = {
                        technician : {},
                        pharmacy : pharmacy[0]
                    };
                    render.renderHtml(res, view.views["admin"]["edit_technician_admin"], data);
                    return;
                }
                technicianBusiness.getOne(parseInt(qs.id), function (technician) {
                    if(technician.length==0){
                        var data = {
                            technician : {},
                            pharmacy : {}
                        };
                        render.renderHtml(res, view.views["admin"]["edit_technician_admin"], data);
                        return;
                    }
                    pharmacyBusiness.getPharmacyById(technician[0].PharmacyId, function (pharmacy) {
                        var data = {
                            technician : technician[0],
                            pharmacy:pharmacy[0]
                        };
                        render.renderHtml(res, view.views["admin"]["edit_technician_admin"], data);
                    });
                });
            }
        });
    },
    upsert_technician_admin : function(req, res){
        var cookies = core.parseCookies(req);
        var token = cookies.token;
        if (token == undefined) {
            core.redirect(res, '/login');
            return;
        }
        
        req.on('end', function(){
            technicianBusiness.upsert(req.formData, function(result){
                if(result){
                    render.renderData(res, { status: "success" });
                }else{
                    render.renderData(res, { status: "error" });
                }
            });
        });
    },
    delete_technician: function(req, res){
        var cookies = core.parseCookies(req);
        var token = cookies.token;
        if (token == undefined) {
            core.redirect(res, '/login');
            return;
        }
        
        req.on('end', function(){
            technicianBusiness.delete(req.formData, function(result){
                if(result){
                    render.renderData(res, { status: "success" });
                    return;
                }else{
                    render.renderData(res, { status: "error" });
                    return;
                }
            });
        });
    }
};

module.exports= admin;