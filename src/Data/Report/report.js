var pg = require('./../Repository/postgre');


var workReport = {
    getAnswerReport: function (startDate, endDate, cb, cbErr) {
        let sDateArr = startDate.split('/');
        let sDate = `${sDateArr[2]}-${sDateArr[0]}-${sDateArr[1]}`;

        let eDateArr = endDate.split('/');
        let eDate = `${eDateArr[2]}-${eDateArr[0]}-${eDateArr[1]} 23:59:59`;
        const query = {
            text: `
            select s."Name" as Urun,
                    p."Name" as Eczane,
                    t."Name" as Teknisyen,                   
                    (select count(*) from "SurveyQuestions" where "SurveyId" = s."ProductId") as QuestionCount,
                    (select count(*) from "SurveyAnswers" sa
                inner join "SurveyQuestionsOptions" sqo on sa."OptionId" = sqo."Id"
                where "SendedSurveyId" = ss."Id" and sqo."IsCorrect" = 1) as "CorrectAnswerCount",
                    (select sa."CreatedAt" from "SurveyAnswers" sa
                inner join "SurveyQuestionsOptions" sqo on sa."OptionId" = sqo."Id"
                where "SendedSurveyId" = ss."Id" limit 1),
                U."Name",
                ss."CreatedAt" as "StartedDate"
                    from "SendedSurveys" ss
                inner join "Survey" s on ss."SurveyId" = s."Id"
                inner join "Pharmacies" p on ss."PharmacyId" = p."Id"
                inner join "Users" U on ss."CreatedBy" = U."Id"
                left  join "Technician" t on t."Id" = ss."TechnicianId"
            where ss."IsFinised" = 1
            and ss."CreatedAt" > $1
            and ss."CreatedAt" < $2`,//2022-12-15 23:59:59
            values: [sDate, eDate]
        };
        pg.query(query, function (result) {
            cb && cb(result);
        }, function (err) {
            cbErr && cbErr(err);
        });
    },
    getCheckinReport: function (startDate, endDate, cb, cbErr) {
        let sDateArr = startDate.split('/');
        let sDate = `${sDateArr[2]}-${sDateArr[0]}-${sDateArr[1]}`;

        let eDateArr = endDate.split('/');
        let eDate = `${eDateArr[2]}-${eDateArr[0]}-${eDateArr[1]} 23:59:59`;
        let query = {
            text: `
            select c."CreatedAt", p."Name" as "Eczane", u."Name" as "Fud", c."lat", c."lng" from "CheckIns" c
                inner join "Pharmacies" p on p."Id" =c."PhrmacyId"
                inner join "Users" u on u."Id" = c."UserId"
            where c."CreatedAt" > $1
            and c."CreatedAt" < $2`,
            values: [sDate, eDate]
        };
        pg.query(query, function (result) {
            cb && cb(result);
        }, function (err) {
            cbErr && cbErr(err);
        });
    },
    fudPharmacyTech: function (cb, cbErr) {
        
        let query = {
            text: `
            select distinct u."Name" Fud , p."Name" Eczane, "Contact" Eczaci, t."Name" Tech  from "Pharmacies" p
                inner join "Users" u on (p.fudid = u."Id")
                inner join "Technician" t on (t."PharmacyId" = p."Id")
            where p."Status" =1 and u."Status" =1 and t."Status" =1 
            order by u."Name" `,
            values: []
        };
        pg.query(query, function (result) {
            cb && cb(result);
        }, function (err) {
            cbErr && cbErr(err);
        });
    },
    checkInReportForFud: function(fudId, startDate, endDate, cb, cbErr){
        let sDateArr = startDate.split('/');
        let sDate = `${sDateArr[2]}-${sDateArr[0]}-${sDateArr[1]}`;

        let eDateArr = endDate.split('/');
        let eDate = `${eDateArr[2]}-${eDateArr[0]}-${eDateArr[1]} 23:59:59`;
        let query = {
            text: `
            select c."CreatedAt", p."Name" as "Eczane", u."Name" as "Fud", c."lat", c."lng" from "CheckIns" c
                inner join "Pharmacies" p on p."Id" =c."PhrmacyId"
                inner join "Users" u on u."Id" = c."UserId"
            where c."CreatedAt" > $1
            and c."CreatedAt" < $2
            and u."Id" = $3`,
            values: [sDate, eDate, fudId]
        };
        pg.query(query, function (result) {
            cb && cb(result);
        }, function (err) {
            cbErr && cbErr(err);
        });
    }

};

module.exports = workReport;