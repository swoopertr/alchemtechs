console.log('admin_fud_savepharmacy_page.js');
userPageType = 2;
check_login();

(function () {
var isLocationAvailable = false;

    function getLocation() {
        if (navigator.geolocation) {
            isLocationAvailable = true;
            navigator.geolocation.getCurrentPosition(showPosition, showError);
        } else {
            isLocationAvailable = false;
            document.getElementById('btnCheckin').disabled = true;
            //alert("Geolocation is not supported by this browser.");
            alert("Tarayıcı tarafından lokasyon bilgisi desteklenmiyor.");
        }
    }

    function showPosition(position) {
        console.log(position.coords.latitude + ", " + position.coords.longitude);
        document.getElementById('coordinate').value = position.coords.latitude + "," + position.coords.longitude;
    }

    function showError(error) {
        document.getElementById('btnCheckin').disabled = true;
        isLocationAvailable = false;
        switch(error.code) {
            case error.PERMISSION_DENIED:
                console.log("User denied the request for Geolocation.");
                alert('Lokasyon verisi isteğini red ettiniz.')
              break;
            case error.POSITION_UNAVAILABLE:
                console.log("Location information is unavailable.");
                alert('Lokasyon verisi erişilebilir değil.')
              break;
            case error.TIMEOUT:
                console.log("The request to get user location timed out.");
                alert('Lokasyon verisi isteği zaman aşımına uğradı')
              break;
            case error.UNKNOWN_ERROR:
                console.log("An unknown error occurred.");
                alert('Bilinmeyen bir hata alındı.');
              break;
          }
    }

    getLocation();
    document.getElementById('btnCheckin').addEventListener('click', function (e) {
        e.preventDefault();
        e.target.disabled = true;
        
        savePharmacy(function(result){
            if(result != "0") {
                alert('Kayıt işlemi başarılı. Anasayfaya yönlendiriliyorsunuz.');
                window.location.href = '/fud';
            }
        });
    });

    function savePharmacy(cb) {

        var pharmacyName = $('#pharmacyName').val();
        var nameSurname = $('#nameSurname').val();
        var gsm = $('#gsm').val();
        var officePhone = $('#officePhone').val();
        var email = $('#email').val();
        var glnCode = $('#glnCode').val();
        var country = $('#country').val();
        var city = $('#city').val();
        //var street = $('#street').val();
        var town = $('#town').val();
        var coordinate = $('#coordinate').val();
        //var quarter = $('#quarter').val();
        //var technician = $('#nameTechnician').val();
        var adress = $('#adress').val();

        var coordinates = coordinate.split(',');
        var lat = coordinates[0];
        var lng = coordinates[1];

        if(pharmacyName == ''){alert('Eczane ismi giriniz'); document.getElementById('btnCheckin').disabled = false; return;}
        if(nameSurname == ''){alert('Eczacı ismi giriniz'); document.getElementById('btnCheckin').disabled = false; return;}
        if(gsm == ''){alert('GSM giriniz'); document.getElementById('btnCheckin').disabled = false; return;}
        if(officePhone == ''){alert('Telefon giriniz'); document.getElementById('btnCheckin').disabled = false; return;}
        if(country == ''){alert('Ülke giriniz'); document.getElementById('btnCheckin').disabled = false; return;}
        if(city == ''){alert('Şehir giriniz'); document.getElementById('btnCheckin').disabled = false; return;}
        if(town == ''){alert('İlçe giriniz'); document.getElementById('btnCheckin').disabled = false; return;}
        if(adress == ''){alert('Adres giriniz'); document.getElementById('btnCheckin').disabled = false; return;}

        var post_obj = {
            pharmacyName,
            nameSurname,
            officePhone,
            gsm,
            lng,
            lat,
            email,
            glnCode,
            country,
            city,
            town,
            adress
            //technician
        };
     

        var token = util.cookie.get('token');
        var check_obj = [
            { name: 'token',  value: token },
            { name: 'userPageType',  value: userPageType }
        ];

        util.postJsonRequest('/fud/savepharmacy', post_obj, check_obj, function (result) {
            if (result != "0") {
                cb && cb(result);
            } 
        });
    }

    document.getElementById('email').addEventListener('blur', function (e) {
        var email_value =  e.target.value;
       
        check_pharmacy_email(email_value, function(result){
            if(result == "true"){
                e.target.style.background="red";
                document.getElementById('btnCheckin').disabled = true;
            }else{
                e.target.style.background="none";
                document.getElementById('btnCheckin').disabled = false;
            }
        });
    });

    function check_pharmacy_email(email, cb){

        var token = util.cookie.get('token');
        var check_obj = [
            { name: 'token',  value: token },
            { name: 'userPageType',  value: userPageType }
        ];

        var post_obj = {
            email
        }

        util.postJsonRequest('/fud/checkpharmacyemail', post_obj, check_obj, function (result) {
            cb && cb(result); 
        });
    }

})();