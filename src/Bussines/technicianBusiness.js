var pharmacyData = require('./../Data/Pharmacy/pharmacy');
var technicianData = require('./../Data/Technician/technician');

var technicianBusiness = {
    getAll: function(cb){
        technicianData.getAll(function (result) {
            cb && cb(result);
        });
    },
    getPharmacyId: function(PharmacyId, cb){
        technicianData.getByPharmacyId(PharmacyId, function (result) {
            cb && cb(result);
        });
    },
    getOne : function(id, cb){
        technicianData.getOne(id, function(result){
            cb && cb(result);
        });
    },
    upsert: function(data, cb){
        if(data.Id == 0){
            technicianData.insert(data, function (result) {
                cb && cb(result);
            });
        }else{
            technicianData.update(data, function (result) {
                cb && cb(result);
            });
        }
    },
    delete : function(data, cb){
        technicianData.delete(data, function(result){
            cb && cb(result);
        });
    },
    getHash : function(hash, cb){
        technicianData.getHash(hash, function(result){
            cb && cb(result);
        });
    },
    verifyKvkk : function(hash, cb){
        technicianData.verifyKvkk(hash, function(result){
            cb && cb(result);
        });
    },
    update_last : function(data, cb){
        technicianData.update_last(data, function(result){
            cb && cb(result);
        });
    },
    async : {
        upsert : function(data){
            return new Promise((resolve, reject)=>{
                technicianBusiness.upsert(data, function(result){
                    resolve(result);
                });
            });
        },
        getAll: function(){
            return new Promise((resolve, reject)=>{
                technicianBusiness.getAll(function(result){
                    resolve(result);
                });
            });
        },
        getPharmacyId : function(PharmacyId){
            return new Promise((resolve, reject)=>{
                technicianBusiness.getPharmacyId(PharmacyId, function(result){
                    resolve(result);
                });
            });
        },
        getOne : function (id) {
            return new Promise((resolve, reject)=>{
                technicianBusiness.getOne(id, function(result){
                    resolve(result);
                });
            }); 
        },
        getHash : function (hash) {
            return new Promise((resolve, reject)=>{
                technicianBusiness.getHash(hash, function(result){
                    resolve(result);
                });
            }); 
        },
        verifyKvkk : function (hash) {
            return new Promise((resolve, reject)=>{
                technicianBusiness.verifyKvkk(hash, function(result){
                    resolve(result);
                });
            }); 
        },
        update_last : function (data) {
            return new Promise((resolve, reject)=>{
                technicianBusiness.update_last(data, function(result){
                    resolve(result);
                });
            });
        }
    }
};

module.exports = technicianBusiness;