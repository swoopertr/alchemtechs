

document.getElementById("btnSend").onclick = function (e) {
    let isAgreement = document.getElementById("agreement").checked;
    if (!isAgreement) {
        alert("Lütfen KvKK sözleşmesini okuyup kabul ediniz.");
        return;
    }else{
        let id = document.getElementById("id").value;
        let email = document.getElementById("email").value;
        let name = document.getElementById("name").value;
        let surname = document.getElementById("surname").value;
        let phone = document.getElementById("phone").value;
        let agreement = document.getElementById("agreement").checked;
        var data = {
            id,
            email,
            name,
            surname,
            phone,
            agreement
        };
        util.postJsonRequest('verifykvkk', data, undefined, function (result) {
            var result= JSON.parse(result);
            console.log(result);
            if(result.status){
                alert("Onay verdiğiniz için teşekkür ederiz");
                window.location.href = 'https://www.alchemlife.com.tr/';
            }
        });
        
    }

};