console.log("pharmacy_list.js");
userPageType = 1;
check_login();

(function() {


    let html_tr_template = `<tr data-id="{{Id}}" >
    <td>{{Name}}</td>
    <td>{{fud_name}}</td>
    <td>{{Contact}}</td>
    <td>{{Phone}}</td>
    <td>{{CellPhone}}</td>
    <td>
        <a href="/admin_pharmacy_technicians?id={{Id}}" class="btn btn-primary">Teknisyenler</a>
        <span> </span>
        <a href="/admin/pharmacy-edit?id={{Id}}" class="btn btn-primary">Düzenle</a>
        <a href="#"  class="btn btn-danger btn-delete" data-id="{{Id}}">Sil</a>
    </td>
 </tr> `;
 util.event.bindLive('.btn-delete', 'click', function(e){
        e.preventDefault();      
        var isDelete = confirm('Eczane silmek istediğinize emin misiniz?');
        if(isDelete){
            var id = e.target.getAttribute('data-id');
            util.postJsonRequest('/admin/delete_pharmacy', {id:id}, undefined, function (result) {
                if(JSON.parse(result).status == "success"){
                    window.location.href = '/admin/pharmacy-list';
                }else{
                    console.log(result);    
                }
            });
        }
    });   

    document.getElementById('searchPharmacyText').addEventListener('keyup', function (e) {
        let searchText = e.target.value;
        if(searchText.length > 2){
            search(searchText);
        }else if(searchText.length == 0){
            loadAll();
        }
        
    });

    function search(searchText){
        let all_elements = document.querySelectorAll('#datatable-tabletools tr[data-id]');
        for (let i = 0; i < all_elements.length; i++) {
            const element = all_elements[i];
            element.remove();
        }
       
        let result_html = '';
        for (let i = 0; i < pharmacies.length; i++) {
            const item = pharmacies[i];
            if(item.Name.toLowerCase().indexOf(searchText.toLowerCase()) != -1){
                let tmp_html = html_tr_template.replace(new RegExp('{{Id}}', 'g'), item.Id);
                tmp_html = tmp_html.replace(new RegExp('{{Name}}','g'), item.Name);
                tmp_html = tmp_html.replace(new RegExp('{{fud_name}}','g'), item.fud_name);
                tmp_html = tmp_html.replace(new RegExp('{{Contact}}','g'), item.Contact);
                tmp_html = tmp_html.replace(new RegExp('{{Phone}}','g'), item.Phone);
                tmp_html = tmp_html.replace(new RegExp('{{CellPhone}}','g'), item.CellPhone);
                result_html += `<tr>${tmp_html}</tr>`; 
            }
            
        }
        document.querySelector('#pharmacies_list').innerHTML= result_html;
    }

    function loadAll(){
        let result_html = '';
            for (let i = 0; i < pharmacies.length; i++) {
                const item = pharmacies[i];
                
                let tmp_html = html_tr_template.replace(new RegExp('{{Id}}', 'g'), item.Id);
                tmp_html = tmp_html.replace(new RegExp('{{Name}}','g'), item.Name);
                tmp_html = tmp_html.replace(new RegExp('{{fud_name}}','g'), item.fud_name);
                tmp_html = tmp_html.replace(new RegExp('{{Contact}}','g'), item.Contact);
                tmp_html = tmp_html.replace(new RegExp('{{Phone}}','g'), item.Phone);
                tmp_html = tmp_html.replace(new RegExp('{{CellPhone}}','g'), item.CellPhone);
            
                result_html += `<tr>${tmp_html}</tr>`; 
            }
            document.querySelector('#pharmacies_list').innerHTML= result_html;
    }


})();
