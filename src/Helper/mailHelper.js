const nodemailer = require('nodemailer');
let settings= require('../Config/setting');

let mailManaager = {
    send_mail: function (to, topic, body, cb) {
        const transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
              user: settings.emailConfig.email,
              pass: settings.emailConfig.pass
            }
            //logger: true
          });
          
        let mailDetails = {
            from: `AlchemLifeEgitim <${settings.emailConfig.email}>`,
            to: to,
            subject: topic,
            html: body
        };

          transporter.sendMail(mailDetails , function(err, data) {
            if(err){
              console.log(err);
              cb && cb({status: 'err'});
              return;
            }
            cb && cb(data);
          });
    },
    send_mail_async: function (to, topic, body) {
      return new Promise((resolve, reject) => {
        mailManaager.send_mail(to, topic, body, function (result) {
          resolve(result);
        });
      });
    }
};


module.exports = mailManaager;