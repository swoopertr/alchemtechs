var render = require('./../Middleware/render');
var view = require('./../Middleware/ViewPack');
var header = require('./../Middleware/header');
var userBusiness = require('./../Bussines/userBusiness');
var adminBussiness = require('./../Bussines/adminBusiness');
var reportBussines = require('./../Bussines/reportBusiness');
var core = require('./../Core');
var url = require('url');
var fs = require('fs');
var settings = require('./../Config/setting');

var report = {
    survey_report: function (req, res) {
        var cookies = core.parseCookies(req);
        var token = cookies.token;
        if (token == undefined) {
            core.redirect(res, '/login');
            return;
        }
        let qs = url.parse(req.url, true).query;
        if(!qs.startDate){
            render.renderHtml(res, view.views["report"]["survey_report"], {list:[]});
            return;
        }else if(!qs.endDate){
            render.renderHtml(res, view.views["report"]["survey_report"], {list:[]});
            return;
        }
        reportBussines.getAnswerReport(qs.startDate, qs.endDate, function (result) {
            var data = {
                list: result
            };
            render.renderHtml(res, view.views["report"]["survey_report"], data);
        },function(err){
            render.renderData(res, {err});
        });
    },
    report_checkin: function (req, res) {
        var cookies = core.parseCookies(req);
        var token = cookies.token;
        if (token == undefined) {
            core.redirect(res, '/login');
            return;
        }
        let qs = url.parse(req.url, true).query;
        if(!qs.startDate){
            render.renderHtml(res, view.views["report"]["survey_report"], {list:[]});
            return;
        }else if(!qs.endDate){
            render.renderHtml(res, view.views["report"]["survey_report"], {list:[]});
            return;
        }

        reportBussines.getCheckinReport(qs.startDate, qs.endDate,function (result) {
            var data = {
                list: result
            };
           
            render.renderHtml(res, view.views["report"]["checkin_report"], data);
            
        });
    },
    report_fudpharmacytech: function(req, res){
        var cookies = core.parseCookies(req);
        var token = cookies.token;
        if (token == undefined) {
            core.redirect(res, '/login');
            return;
        }
        reportBussines.fudPharmacyTech((result)=>{
            var data = {
                list: result
            };

            render.renderHtml(res, view.views["report"]["fudPharmacyTech_report"], data);
        });
    },

    checkin_report_for_fud : function(req, res){
        var cookies = core.parseCookies(req);
        var token = cookies.token;
        if (token == undefined) {
            core.redirect(res, '/login');
            return;
        }
        let qs = url.parse(req.url, true).query;
        if(!qs.startDate){
            render.renderHtml(res, view.views["report"]["survey_report_fud"], {list:[]});
            return;
        }else if(!qs.endDate){
            render.renderHtml(res, view.views["report"]["survey_report_fud"], {list:[]});
            return;
        }
        userBusiness.checkToken(token, (userInfo )=>{
            
            reportBussines.checkInforFud(userInfo.userId, qs.startDate, qs.endDate, (result)=>{
                var data = {
                    list: result
                };
    
                render.renderHtml(res, view.views["report"]["survey_report_fud"], data);
            }); 
        });
    }
   
};


module.exports = report;