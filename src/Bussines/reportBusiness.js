const core = require('../Core');
var data_report = require('./../Data/Report/report');
var fs = require('fs');

var bussines_report = {
    getAnswerReport: function (startDate, endDate, cb, cbErr) {
        data_report.getAnswerReport(startDate, endDate, function (result) {
            for (var i = 0; i < result.length; i++) {
                let item = result[i];
                item.CreatedAt = item.CreatedAt.addHours(3);
                
                let date_create_obj = core.formatDate(item.CreatedAt);
                item.CreatedAt = date_create_obj.full;
                item.CreatedAtDate = date_create_obj.date;
                item.CreatedAttime = date_create_obj.time;
                item.StartedDate = item.StartedDate.addHours(3);
                
                let date_start_obj = core.formatDate(item.StartedDate);
                item.StartedDate = date_start_obj.full;
                item.StartedDateDate =date_start_obj.date;
                item.StartedTime = date_start_obj.time;
                
                result[i] = item;
            }

            cb && cb(result);
        }, function (err) {
            cbErr && cbErr(err);
        });
    },
    getCheckinReport: function(startDate, endDate, cb, cbErr){
        data_report.getCheckinReport(startDate, endDate, function(result){
            for (var i = 0; i < result.length; i++) {
                let item = result[i];
                let date_obj = core.formatDate(item.CreatedAt.addHours(3));
                item.CreatedAt = date_obj.full;
                item.CreatedAtDate = date_obj.date;
                item.CreatedAttime = date_obj.time;
                result[i] = item;
            }
            
            cb && cb(result);
        }, function(err){
            cbErr && cbErr(err);
        });
    },
    fudPharmacyTech: function(cb, cbErr){
        data_report.fudPharmacyTech(cb, cbErr);
    },
    checkInforFud: function(fudId, startDate, endDate, cb, cbErr){
        data_report.checkInReportForFud(fudId, startDate, endDate, (result)=>{
            for (let i = 0; i < result.length; i++) {
                let item = result[i];
                let date_obj = core.formatDate(item.CreatedAt.addHours(3));
                item.CreatedAtDate = date_obj.date;
                item.CreatedAttime = date_obj.time;
                result[i] = item;
            }
            cb && cb(result);
        } , cbErr);
    },
    async: {
      
    }
};

module.exports = bussines_report;