var render = require('./../Middleware/render');
var view = require('./../Middleware/ViewPack');
var exuserBusiness = require('./../Bussines/exuserBusiness');
var technicianBusiness = require('./../Bussines/technicianBusiness');
var url = require('url');
var core = require('./../Core');

var technician = {
    index: function (req, res) {

        var cookies = core.parseCookies(req);
        var token = cookies.token;
        if(token == undefined){
            core.redirect(res, '/exlogin');
        }

        exuserBusiness.checkToken(token, function(result){
            if(result == false){
                core.redirect(res, '/exlogin');
            }else{
                exuserBusiness.calculatePoints(result[0].Id, function(result){
                    render.renderHtml(res, view.views["technician"]["index"], {result});
                }, function(err){});
            }
             
        });
    },
    check_token: function (req, res) {
        var cookies = core.parseCookies(req);
        var token = cookies.token;
        if(token == undefined){
            core.redirect(res, '/exlogin');
        }else{
            exuserBusiness.checkToken(token, function(result){
                if(result == false){
                    core.redirect(res, '/exlogin');
                }else{
                    render.renderData(res, {
                        status: "success"
                    }, 'json');
                }
            });
        }
    },
    surveys: function(req, res){
        var cookies = core.parseCookies(req);
        var token = cookies.token;
        if(token == undefined){
            core.redirect(res, '/exlogin');
        }else{
            exuserBusiness.checkToken(token, function(result){
                if(result == false){
                    core.redirect(res, '/exlogin');
                }else{
                    exuserBusiness.getAllSurveysByPharmacyId(result[0].Id ,function(result){
                        var data = {
                            surveys: result
                        };
                        render.renderHtml(res, view.views["technician"]["surveys"], {data});
                    });
                }
            }, function(err){
                core.redirect(res, '/exlogin');
            });
        }
    },
    feedback: function(req, res){
        var cookies = core.parseCookies(req);
        var token = cookies.token;
        if(token == undefined){
            core.redirect(res, '/exlogin');
        }else{
            exuserBusiness.checkToken(token, function(result){
                if(result == false){
                    core.redirect(res, '/exlogin');
                }else{
                    render.renderHtml(res, view.views["technician"]["feedback"], {});
                }
            }, function(err){
                render.renderData(res, {
                    status: "fail"
                }, 'json');
            });
        }
    },
    feedback_post: function(req, res){
        var cookies = core.parseCookies(req);
        var token = cookies.token;
        if(token == undefined){
            core.redirect(res, '/exlogin');
            return;
        }

        req.on('end', function(){
            exuserBusiness.checkToken(token, function(result){
                if(result == false){
                    core.redirect(res, '/exlogin');
                }else{
                    exuserBusiness.addFeedback(req.formData, function(result){
                        render.renderData(res, {
                            status: "success"
                        }, 'json');
                    }, function(err){
                        core.redirect(res, '/exlogin');
                    });
                }
            }, function(err){
                core.redirect(res, '/exlogin');
            });
        });
    },
    point_report: function(req, res){
        var cookies = core.parseCookies(req);
        var token = cookies.token;
        if(token == undefined){
            core.redirect(res, '/exlogin');
            return;
        }

        exuserBusiness.checkToken(token, function(result){
            if(result == false){
                core.redirect(res, '/exlogin');
            }else{
                //todo calculate points
                exuserBusiness.calculatePoints(result[0].Id, function(result){
                   
                    render.renderHtml(res, view.views["technician"]["pointreport"], {result});
                }, function(err){

                });
                
            }
        }, function(err){
            render.renderData(res, {
                status: "fail"
            }, 'json');
        });

    },
    verify_kvkk: function(req, res){
        let qs = url.parse(req.url, true).query;
        let hash = qs.hash;
        if(hash == undefined){
            let result = {status: 'fail', message : 'there is no hash in url'};
            render.renderData(res, result, 'json');
            return;
        }
        technicianBusiness.getHash(hash,async (result)=>{
            
            if(result.length == 0){
                render.renderData(res, text, 'no token exist');
            }else{
                result = result[0];
                let status = false;
                let text ='';
                if(result.kvkkverify == 1){
                    text = `Sayın ${result.Name} ${result.Surname},</br></br> KVKK (Kişisel Verilerin Korunması Kanunu) ile ilgili daha önce onayınızı alarak, sizinle güvenli ve yasal bir şekilde iletişim kurmamıza olanak sağlamışsınız.</br></br> Teşekkürler`;
                    status = false;
                
                }else if(result.kvkkhashexpiredate < new Date()){
                    text = `Link süresi dolmuştur. İlginiz için teşekkür ederiz.`;
                    status = false;
                }else{
                    //await technicianBusiness.async.verifyKvkk(result.kvkkhash);
                    text = `Sayın ${result.Name} ${result.Surname}, KVKK onayı vermeden önce, lütfen iletişim bilgilerinizi güncelleyin.`;
                    status = true;
                }
                let data = {
                    text,
                    result,
                    status
                };
                render.renderHtml(res, view.views["technician"]["kvkkverify"], data);
                return;
            }
        });
    },
    verify_kvkk_post: (req, res)=>{
        req.on('end', async ()=>{
            let data = req.formData;
            let techData = await technicianBusiness.async.getOne(data.id);
            if(data.agreement){
                await technicianBusiness.async.verifyKvkk(techData[0].kvkkhash);
                let dto = {
                    Id : data.id, 
                    email: data.email, 
                    name :data.name, 
                    phone : data.phone, 
                    surname : data.surname
                };
                await technicianBusiness.async.update_last(dto);
                render.renderData(res, {status: 'success'}, 'json');
            }else{
                render.renderData(res, {status: 'fail'}, 'json');
            }
            
        });
    }
}

module.exports = technician;