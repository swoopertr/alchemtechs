var pg = require('../Repository/postgre');

var technician_work = {
    getAll :function(cb, cbErr) {
        let query = {
            text: `SELECT * FROM public."Technician" order by Id;`,
            values: []
        };
       
        pg.query(query, function (result) {
            cb && cb(result);
        }, function (err) {
            cbErr && cbErr(err);
        });
    },
    getAllActive : function(cb, cbErr){
        let query = {
            text: `SELECT * FROM public."Technician" where "Status" = 1 and "Id">2882 and "Id"<2982 and "Email" <> '' order by "Id";`,
            values: []
        };
       
        pg.query(query, function (result) {
            cb && cb(result);
        }, function (err) {
            cbErr && cbErr(err);
        });
    },
    getByPharmacyId : function(PharmacyId, cb, cbErr){
        let query = {
            text: `SELECT * FROM public."Technician" where "PharmacyId" = $1;`,
            values: [PharmacyId]
        };
       
        pg.query(query, function (result) {
            cb && cb(result);
        }, function (err) {
            cbErr && cbErr(err);
        });
    },
    getOne : function(id, cb, cbErr){
        let query = {
            text: `SELECT * FROM public."Technician" where "Id" = $1;`,
            values: [id]
        };
       
        pg.query(query, function (result) {
            cb && cb(result);
        }, function (err) {
            cbErr && cbErr(err);
        });
    },
    getHash : function(hash, cb, cbErr){
        let query = {
            text: `SELECT * FROM public."Technician" where "kvkkhash" = $1;`,
            values: [hash]
        };
       
        pg.query(query, function (result) {
            cb && cb(result);
        }, function (err) {
            cbErr && cbErr(err);
        });
    },
    verifyKvkk : function(hash, cb, cbErr){
        let query = {
            text: `update public."Technician" set "kvkkverify"=1, kvkkanswerdate=now()  where "kvkkhash" = $1;`,
            values: [hash]
        };
       
        pg.query(query, function (result) {
            cb && cb(result);
        }, function (err) {
            cbErr && cbErr(err);
        });
    },
    insert : function(data, cb, cbErr){
        let query = {
            text : `INSERT INTO public."Technician"("Email", "Name", "Phone", "Status", "CreatedAt", "PharmacyId")
                                            VALUES ($1,      $2,     $3,      $4,       now(),       $5);`,
            values: [data.email, data.nameSurName, data.phone, data.status, data.PharmacyId]
        };
        pg.query(query, function (result) {
            cb && cb(result);
        }, function (err) {
            cbErr && cbErr(err);
        });
    },
    update: function(data, cb, cbErr){
        let query = {
            text : `UPDATE public."Technician" SET "Email"=$2, "Name"=$3, "Phone"=$4, "Status"=$5, "PharmacyId"=$6 WHERE "Id"=$1;`,
            values: [data.Id, data.email, data.nameSurName, data.phone, data.status, data.PharmacyId]
        };

        pg.query(query, function (result) {
            cb && cb(result);
        }, function (err) {
            cbErr && cbErr(err);
        });
    },

    update_last: function(data, cb, cbErr){
        let query = {
            text : `UPDATE public."Technician" SET "Email"=$2, "Name"=$3, "Phone"=$4, "Surname"=$5 WHERE "Id"=$1;`,
            values: [data.Id, data.email, data.name, data.phone, data.surname]
        };

        pg.query(query, function (result) {
            cb && cb(result);
        }, function (err) {
            cbErr && cbErr(err);
        });
    },

    test_update: function(data, cb, cbErr){
        let query = {
            text : `UPDATE public."Technician" SET "Email"=$2, "Name"=$3, "Phone"=$4, "Status"=$5, "PharmacyId"=$6, "Surname"= $7 WHERE "Id"=$1;`,
            values: [data.Id, data.Email, data.Name, data.Phone, data.Status, data.PharmacyId, data.Surname]
        };

        pg.query(query, function (result) {
            cb && cb(result);
        }, function (err) {
            cbErr && cbErr(err);
        });
    },
    delete: function(data, cb,cbErr){
        let query = {
            text : `UPDATE public."Technician" SET "Status"=3 WHERE "Id"=$1;`,
            values: [data.Id]
        };
        pg.query(query, function (result) {
            cb && cb(result);
        }, function (err) {
            cbErr && cbErr(err);
        });
    },
    async:{
        getAll : function () {
            return new Promise(function (resolve, reject) {
                technician_work.getAll(function (result) {
                    resolve(result);
                },
                function (err) {
                    reject(err);
                });
            });
        },
        getAllActive:  function () {
            return new Promise(function (resolve, reject) {
                technician_work.getAllActive(function (result) {
                    resolve(result);
                },
                function (err) {
                    reject(err);
                });
            });
        },
        getByPharmacyId: function(PharmacyId){
            return new Promise(function (resolve, reject) {
                technician_work.getByPharmacyId(PharmacyId, function (result) {
                    resolve(result);
                },
                function (err) {
                    reject(err);
                });
            });
        },
        getOne: function(id){
            return new Promise(function (resolve, reject) {
                technician_work.getOne(id, function (result) {
                    resolve(result);
                },
                function (err) {
                    reject(err);
                });
            });
        },
        insert : function(data){
            return new Promise(function (resolve, reject) {
                technician_work.insert(data, function (result) {
                    resolve(result);
                },
                function (err) {
                    reject(err);
                });
            });
        },
        test_update : function(data){
            return new Promise(function (resolve, reject) {
                technician_work.test_update(data, function (result) {
                    resolve(result);
                },
                function (err) {
                    reject(err);
                });
            });
        },
        update_last: function(data){
            return new Promise(function (resolve, reject) {
                technician_work.update_last(data, function (result) {
                    resolve(result);
                },
                function (err) {
                    reject(err);
                });
            });
        }
    }
};
module.exports = technician_work;