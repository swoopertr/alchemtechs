console.log('admin_survey_report.js');
userPageType = 1;
check_login();

(function() {
    var startDate_dom =document.getElementById('startDate');
    var endDate_dom = document.getElementById('endDate');
    if(location.search != ''){
        let qsobj = util.qsObject();
        if(qsobj.startDate){
            startDate_dom.value = qsobj.startDate;
        }
        if (qsobj.endDate){
            endDate_dom.value = qsobj.endDate;
        }
        let tmp_href = `/admin/answers/excel?startDate=${qsobj.startDate}&endDate=${qsobj.endDate}`;
        document.getElementById('btn-excel').setAttribute('href',tmp_href);
        
    }
    
    document.getElementById('btnPickDate').addEventListener('click', function (e) {
        e.preventDefault();
        console.log('clicked endDate',endDate_dom.value);
        console.log('clicked startDate',startDate_dom.value);
        location.search = "startDate="+startDate_dom.value+"&endDate="+endDate_dom.value;
        let tmp_href = `/admin/answers/excel?startDate=${startDate_dom.value}&endDate=${endDate_dom.value}`;
        document.getElementById('btn-excel').setAttribute('href',tmp_href);
    })    
})();
